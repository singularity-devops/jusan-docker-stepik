# jusan-docker


## Testing
Before testing, stop all the relevant containers. And in the main folder run
the following commands

```bash
cd ./docker-run && bash ./solution.sh && cd ../
cd ./docker-bind && bash ./solution.sh && cd ../
cd ./docker-mount && bash ./solution.sh && cd ../
cd ./dockerfile && bash ./solution.sh && cd ../
cd ./dockerize && bash ./solution.sh && cd ../
cd ./docker-compose && docker compose up -d && cd ../
cd ./docker-compose-final && docker compose up -d && cd ../
```
```bash
./review-tester.sh
```

## jusan-run Execution

Do the following command to run the nginx in port 80 of container and to map it for 8888 port of the host
```bash
docker run -d --name jsn-dkr-run -p 8888:80 nginx:mainline
```

Or you could run any solution.sh in the given directories
```bash
bash ./solution.sh
```
